'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);
    var rewriteRulesSnippet = require('grunt-connect-rewrite/lib/utils').rewriteRequest;

    grunt.initConfig({

        // Project settings
        main: {
            // configurable paths
            app: require('./bower.json').appPath || 'app',
            dist: 'dist',
            port: 8080,
            livereload: 35729
        },

        // Automatically inject Bower components into the app
        bowerInstall: {
            app: {
                src: ['<%= main.app %>/index.html'],
                ignorePath: '<%= main.app %>/'
            }
        },

        // VALIDATE JS IN CASE OF CODE QUALITY
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            files: [
                'Gruntfile.js',
                '<%= main.app %>/scripts/**/*.js',
                '<%= main.app %>/webgame_components/**/*.js'
            ],
            test: {
                options: {jshintrc: 'test/.jshintrc'},
                src: ['test/**/*.js']
            }
        },

        karma: {
            options: {
                configFile: 'karma.conf.js'
            },
            unit: {
                singleRun: true
            },

            continuous: {
                singleRun: false,
                autoWatch: true
            }
        },

        html2js: {
            dist: {
                src: ['<%= main.app %>/*.html'],
                dest: '.tmp/templates.js'
            }
        },

        concat: {
            options: {
                separator: ';'
            }
        },

        uglify: {
            generated: {
                files: [
                    {
                        dest: '.tmp/concat/scripts/gameScript.js',
                        src: '.tmp/concat/scripts/gameScript.js'
                    },
                    {
                        dest: '.tmp/concat/scripts/bowerScript.js',
                        src: '.tmp/concat/scripts/bowerScript.js'
                    }
                ]
            },
            options: {
                mangle: false
            }
        },

        clean: {
            temp: {
                src: ['.tmp']
            },
            dest: {
                src: ['dist']
            }
        },

        // CONVERT LESS TO CSS IN TEMPFOLDER
        recess: {
            options: {
                compile: true
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= main.app %>/styles/less',
                        src: 'base.less',
                        dest: '<%= main.app %>/styles/css',
                        ext: '.css'
                    }
                ]
            }
        },

        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['bowerInstall']
            },
            js: {
                files: ['<%= main.app %>/**/*.js'],
                tasks: ['jshint'],
                options: {livereload: true}
            },
            jsTest: {
                files: ['test/**/*.js'],
                tasks: ['jshint:test']
            },
            less: {
                files: ['<%= main.app %>/styles/less/**/*.less'],
                tasks: ['clean', 'recess'],
                options: {livereload: true}
            },
            html: {
                files: ['<%= main.app %>/**/*.html'],
                options: {livereload: true}
            },
            gruntfile: {
                files: ['Gruntfile.js'],
                options: {livereload: true}
            }
        },

        connect: {
            options: {
                port: '<%= main.port %>',
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: '0.0.0.0',
                middleware: function (connect, options) {
                    var middlewares = [];
                    if (!Array.isArray(options.base)) {
                        options.base = [options.base];
                    }

                    // RewriteRules support
                    middlewares.push(rewriteRulesSnippet);

                    // Setup the proxy
                    middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);
                    // Serve static files
                    options.base.forEach(function (base) {
                        middlewares.push(connect.static(base));
                    });
                    return middlewares;
                }
            },
            // configuration for task configureProxies from plugin grunt-connect-proxy
            proxies: [{
                context: '/api',
                host: '172.17.42.1', // ip address of the host
                port: 8070,
                changeOrigin: true,
                xforward: false
            }
            ],
            server: {
                options: {
                    open: true,
                    livereload: '<%= main.livereload %>',
                    base: ['tmp', '<%= main.app %>']
                }
            }
        },

        // BUILD Tasks :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: '<%= main.app %>/index.html',
            options: {
                dest: '<%= main.dist %>'
            }
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
            html: ['<%= main.dist %>/index.html'],
            options: {
                assetsDirs: ['<%= main.dist  %>']
            }
        },

        // COPYS FILES FROM APP TO DIST
        copy: {
            components: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= main.app %>',
                        dest: '<%= main.dist %>',
                        src: [
                            '.htaccess',
                            'bower_components/stats.js/build/stats.min.js',
                            'bower_components/threex.rendererstats/threex.rendererstats.js',
                            'webgame_components/**/*.html',
                            'views/**/*.html',
                            'index.html'
                        ]
                    }
                ]
            },
            js: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '.tmp/concat/scripts/',
                        dest: '<%= main.dist %>/scripts/',
                        src: ['*.js']
                    }
                ]
            },
            font: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= main.app %>/bower_components/bootstrap/dist/fonts',
                        dest: '<%= main.dist %>/styles/fonts',
                        src: ['*']
                    }
                ]
            },
            img: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= main.app %>',
                        dest: '<%= main.dist %>',
                        src: ['styles/img/**/*', 'images/placeholder/*']
                    }
                ]
            }
        },

        rev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 4
            },
            dist: {
                files: {
                    src: [
                        '<%= main.dist %>/scripts/**/*.js',
                        '<%= main.dist %>/styles/**/*.css'
                    ]
                }
            }
        },

        cssmin: {
            minify: {
                src: '<%= main.app %>/styles/css/base.css',
                dest: '<%= main.dist %>/styles/css/base.min.css'
            }
        }
    });

    // Loading of tasks and registering tasks will be written here
    grunt.registerTask('default', [
        'build',
        'serve'
    ]);

    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'configureProxies:server', 'configureRewriteRules', 'connect:server:keepalive']);
        }


        grunt.task.run([
            'clean:temp',
            'bowerInstall',
            'configureProxies:server', // added just before connect
            'configureRewriteRules',
            'connect:server',
            'watch'
        ]);

    });

    // not tested
    grunt.registerTask('build', [
        'bowerInstall',
        'clean',
        'recess',
        'jshint',
        'useminPrepare',
        'copy:components',
        'copy:font',
        'copy:img',
        'concat',
        'uglify',
        'copy:js',
        'rev:dist',
        'cssmin',
        'usemin'
    ]);

    grunt.registerTask('test', [
        'karma:unit'
    ]);

    grunt.registerTask('test-dd', [
        'karma:continuous'
    ]);

};
