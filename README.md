#Web Game
=================

A proof of concept

1. [Setup the Frontend Application](#setup-the-frontend-application)

- [Vagrant setup of the Frontend Application](#vagrant-setup-of-the-frontend-application)

- [Start the Frontend Application for developing](#start-the-frontend-application-for-developing)

- [Run Tests](#run-tests)

- [Build the Application](#Build-the-application)

- [Grunt dependencies] (#Grunt-dependencies)


##Setup the Frontend Application

###Install Dependencies

####NodeJS and NPM
NodeJS and NPM are installed. On Mac follow and use [this](http://nodejs.org/).

####Grunt and Bower
Install Grunt, Bower and phantomjs global on your system. That makes easier to use this tools.

  ```
  sudo npm install -g grunt-cli bower phantomjs karma-cli
  ```

###Checkout WebGame-Frontend

  ```
  git clone git@bitbucket.org:enrico_schw/web-game.git
  cd web-game
  ```

###Install components

  ```
  npm install && bower install
  ```
Add dev tools:

  ```
  bower install -s threex.rendererstats=https://github.com/jeromeetienne/threex.rendererstats/archive/master.zip
  ```
  
Now, the application is ready to use.

##Start the Frontend Application for developing
If you want start the application then in a terminal, run the following command from your vm:

  ```
  grunt serve
  ```
  
Now, you can access the Frontend on [http://127.0.0.1:8080/#/](http://127.0.0.1:8080/#/).

##Run Tests

###Run all tests 
To run tests in the project directory, run:

  ```
  grunt test
  ```

###Make Test Driven Development
Doo tdd you should run

  ```
  grunt test-dd
  ```

or run directly

  ```
  karma start karma.conf.js`
  ```

##Build the Application
If you want a runable build of the application then in a terminal, run the following in the project directory:

  ```
  grunt build
  ```
  
Now, the project directory contains a dist directory where you find the complete application


##Grunt Dependencies

The Grunt package contains all logic necessary to run tasks in your project
Normally, the grunt-cli should be installed globally, so you don’t really need it here, but it can’t harm either. To install it globally you use the command:

  ```
  npm install -g grunt-cli
  ```
  
- The grunt-html2js plugin will be used to convert our AngularJS templates (like app/templates/rating.html)
  to JavaScript files, requiring no asynchronous lookups for retrieving the templates

- To minify our files we can use the grunt-contrib-uglify plugin

- We will use the grunt-html2js plugin to convert our template to a JavaScript file, however, we’re also 
  going to concat our JavaScript file, so that file is rather temporary. To clean these temporary files, I’m
  going to use the grunt-contrib-clean plugin

- While developing it would be nice if you could automatically run a webserver to test your application, to 
  do that you can use the grunt-contrib-connect plugin

- When releasing your application, it would be interesting to get an archive file containing all application
  resources, well, you can create such a ZIP file by using the grunt-contrib-compress plugin

- Like explained earlier, we’re going to concatenate our JavaScript files, which is something we can do by 
using the grunt-contrib-concat plugin

- To validate your code quality you can either use JSHint or JSLint (more strict), to integrate this in our 
  build process, we use the grunt-contrib-jshint plugin

- While developing, you want to be able to automatically create your concatenated JavaScript file while 
  developing, the grunt-contrib-watch plugin will execute certain tasks when a file change is detected, 
  great for automatically building your application

- To automatically retrieve the latest Bower dependencies, you can use the grunt-bower-task

- Then finally, the grunt-karma task allows you to execute Karma from within Grunt


Of course, we also have some dependencies or plugins for Karma itself:

- The karma package contains all code necessary to execute KarmaJS

- When using Karma, you also need to define the testing framework you would like to use. In this case I’m 
  going to use Jasmine, so to load all its dependencies we use the karma-jasmine plugin

- In stead of testing your code inside a browser, I’m going to use PhantomJS for headless testing. To 
  integrate it with Karma, we use the karma-phantomjs-launcher plugin

- Then finally, while testing you also want to precompile your template so you can load it in your test 
  cases, similar to what grunt-html2js does, will the karma-ng-html2js-preprocessor be used, but for testing
  purposes.
