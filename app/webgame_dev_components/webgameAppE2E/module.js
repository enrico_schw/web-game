'use strict';

/**
 * @ngdoc overview
 * @name WebGame E2E
 *
 * @description
 * This module mocks the backend http requests.
 *
 * @see https://docs.angularjs.org/api/ngMockE2E/service/$httpBackend
 */
angular.module('webgameAppE2E', ['webgameApp', 'ngMockE2E']);
