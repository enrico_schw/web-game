'use strict';

/**
 * @ngdoc overview
 * @name WebGame game
 *
 * @description
 * This module contains the three js logic for the board
 *
 *
 */
angular.module('boardDevModule', []);
