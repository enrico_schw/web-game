'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace  Camera initialization.  Contains setup for both Perspective and Orthographic cameras.
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.LightHelper = function (params) {

    this.context = params.context;

    // 2. HemisphereLightHelper
    var hlightHelper = new THREE.HemisphereLightHelper(this.context.lights.hemiLight, 50, 300); // 50 is sphere size, 300 is arrow length
    this.context.scene.add(hlightHelper);

    // 3. PointLightHelper
    var pointLightHelper = new THREE.PointLightHelper(this.context.lights.pointLight, 50); // 50 is sphere size
    this.context.scene.add(pointLightHelper);

    // 4. SpotLightHelper
    var spotLightHelper = new THREE.SpotLightHelper(this.context.lights.spotLight, 50); // 50 is sphere size
    this.context.scene.add(spotLightHelper);


    var directionalLight = new THREE.DirectionalLightHelper(this.context.lights.directionalLight, 50); // 50 is helper size
    this.context.scene.add(directionalLight);
};
