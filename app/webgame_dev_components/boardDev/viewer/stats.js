'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace  Camera initialization.  Contains setup for both Perspective and Orthographic cameras.
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.Stats = function (params) {

    this.context = params.context;

    this.statsObj = new Stats();
    this.statsObj.domElement.style.position = 'absolute';
    this.statsObj.domElement.style.top = '0px';
    this.statsObj.domElement.style.zIndex = 100;
    this.context.domElement.append(this.statsObj.domElement);
};

ShigGame.Viewer.Scene.RendererStats = function (params) {

    this.context = params.context;


    this.statsObj = new THREEx.RendererStats();

    this.statsObj.domElement.style.position = 'absolute';
    this.statsObj.domElement.style.left = '0px';
    this.statsObj.domElement.style.bottom = '0px';
    this.context.domElement.append(this.statsObj.domElement);

};
