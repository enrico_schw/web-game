'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace
 * @class Creates a helper Tools
 * @param params
 * @constructor
 */
ShigGame.Viewer.SceneDev = function () {

    this.stats = null;
    this.rendererStats = null;
    this.gridHelper = null;
    this.lightsHelper = null;
    this.axisHelper = null;
    this.cameraHelper = null;
};

ShigGame.Viewer.SceneDev.prototype = new ShigGame.Viewer.Scene();
ShigGame.Viewer.SceneDev.prototype.constructor = ShigGame.Viewer.SceneDev;
ShigGame.Viewer.SceneDev.parent = ShigGame.Viewer.Scene.prototype;


ShigGame.Viewer.SceneDev.prototype.init = function (settings) {

    ShigGame.Viewer.SceneDev.parent.init.call(this, settings);

    var params = {context: this};
    this.stats = new ShigGame.Viewer.Scene.Stats(params);
    this.rendererStats = new ShigGame.Viewer.Scene.RendererStats(params);


    //this.gridHelper = new ShigGame.Viewer.Scene.GridHelper(params);
    //this.lightsHelper = new ShigGame.Viewer.Scene.LightHelper(params);
    //this.axisHelper = new ShigGame.Viewer.Scene.AxisHelper(params);
    //this.cameraHelper = new ShigGame.Viewer.Scene.CameraHelper(params);
};

ShigGame.Viewer.SceneDev.prototype.update = function () {

    ShigGame.Viewer.SceneDev.parent.update.call(this);

    this.stats.statsObj.update();
    this.rendererStats.statsObj.update(this.renderer);
};
