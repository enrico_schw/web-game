'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace
 * @class An axis object to visualize the the 3 axes in a simple way.
 * The X axis is red. The Y axis is green. The Z axis is blue.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.AxisHelper = function (params) {

    this.context = params.context;
    // 2. AxisHelper
    var axisHelper = new THREE.AxisHelper(800); // 500 is size
    this.context.scene.add(axisHelper);
};

ShigGame.Viewer.Scene.ArrowHelper = function (params) {
    this.context = params.context;
    // 1. ArrowHelper
    var directionV3 = new THREE.Vector3(1, 0, 1);
    var originV3 = new THREE.Vector3(0, 200, 0);
    var arrowHelper = new THREE.ArrowHelper(directionV3, originV3, 100, 0xff0000, 20, 10); // 100 is length, 20 and 10 are head length and width
    this.context.scene.add(arrowHelper);
};

