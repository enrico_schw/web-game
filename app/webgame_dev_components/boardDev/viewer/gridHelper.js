'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace
 * @class Creates a helper Grid
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.GridHelper = function (params) {

    this.context = params.context;

    // add helpers:

    // 1. GridHelper
    var gridHelper = new THREE.GridHelper(500, 40); // 500 is grid size, 20 is grid step
    gridHelper.position.set(0, 0, 0);
    //gridHelper.rotation = new THREE.Euler(0, 0, 0);
    this.context.scene.add(gridHelper);

    var gridHelper2 = gridHelper.clone();
    gridHelper2.rotation.x = Math.PI / 2;
    this.context.scene.add(gridHelper2);

    var gridHelper3 = gridHelper.clone();
    gridHelper3.rotation.x = Math.PI / 2;
    gridHelper3.rotation.z = Math.PI / 2;
    this.context.scene.add(gridHelper3);

};
