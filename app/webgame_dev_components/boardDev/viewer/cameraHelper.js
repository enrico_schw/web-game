'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.CameraHelper = function (params) {

    this.context = params.context;

    var cameraHelper = new THREE.CameraHelper(this.context.camera.perspectiveCamera);
    this.context.scene.add(cameraHelper);

};
