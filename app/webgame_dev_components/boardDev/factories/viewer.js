'use strict';

angular.module('boardDevModule').
    factory('ViewerDev', ['GridFactory', function (GridFactory) {

        var scene;

        /**
         * Initialize the 3D scene.
         * @param  {!{ canvasId: string, containerId: string }} params
         */
        function init(params) {

            scene = new ShigGame.Viewer.SceneDev();
            scene.init(params);
            scene.initGrid(GridFactory);
        }

        function addMesh(mesh) {
            scene.add(mesh);
        }

        function render() {
            scene.render();
        }

        function update() {
            scene.update();
        }

        return {
            init: init,
            addMesh: addMesh,
            render: render,
            update: update
        };
    }]);
