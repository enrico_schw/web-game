'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace  Camera initialization.  Contains setup for both Perspective and Orthographic cameras.
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.Lights = function (params) {

    this.context = params.context;

    // Ambient light has no direction, it illuminates every object with the same
    // intensity. If only ambient light is used, no shading effects will occur.
    this.ambientLight = new THREE.AmbientLight(0x101010, 1.0);

    // Directional light has a source and shines in all directions, like the sun.
    // This behaviour creates shading effects.
    this.directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);
    this.directionalLight.position.set(200, 400, 1.0);


    // add hemisphere light
    this.hemiLight = new THREE.HemisphereLight(0x0000ff, 0x00ff00, 0.4);
    this.hemiLight.color.setHSL(0.6, 1, 0.6);
    this.hemiLight.groundColor.setHSL(0.095, 1, 0.75);
    this.hemiLight.position.set(-200, 400, -200);
    this.context.scene.add(this.hemiLight);

    // add point light
    this.pointLight = new THREE.PointLight(0xffff00, 1.0);
    this.pointLight.position.set(300, 300, 300);
    this.context.scene.add(this.pointLight);

    // add spot light
    this.spotLight = new THREE.SpotLight(0xffffff);
    this.spotLight.position.set(-300, 400, 300);
    this.spotLight.castShadow = true;
    this.spotLight.shadowCameraFov = 60;
    this.context.scene.add(this.spotLight);
};
