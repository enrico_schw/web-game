'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace  Camera initialization.  Contains setup for both Perspective and Orthographic cameras.
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.Map = function (params) {

    this.context = params.context;


    // SKYBOX
    var skyBoxGeometry = new THREE.BoxGeometry(1000, 1000, 1000);
    var skyBoxMaterial = new THREE.MeshBasicMaterial({color: 0xffffee, side: THREE.BackSide});
    var skyBox = new THREE.Mesh(skyBoxGeometry, skyBoxMaterial);
    this.context.scene.add(skyBox);


    var axis = new THREE.AxisHelper(33);
    axis.position.y = 0.01;
    this.context.scene.add(axis);

    var squareT = new THREE.ImageUtils.loadTexture('textures/square-thick.png');
    squareT.wrapS = squareT.wrapT = THREE.RepeatWrapping;
    squareT.repeat.set(32, 32);
    var planeGeo = new THREE.PlaneGeometry(32, 32);
    var planeMat = new THREE.MeshBasicMaterial({map: squareT, color: 0xbbbbbb});
    var basePlane = new THREE.Mesh(planeGeo, planeMat);
    basePlane.rotation.x = -Math.PI / 2;
    basePlane.position.set(16, 0, 16);
    basePlane.base = true;
    this.context.scene.add(basePlane);


};

ShigGame.Viewer.Scene.Ground = function (params) {

    this.context = params.context;
    // add simple ground
    var groundGeometry = new THREE.PlaneGeometry(1200, 1200, 1, 1);
    var ground = new THREE.Mesh(groundGeometry, new THREE.MeshLambertMaterial({
        color: 0x9669FE
    }));
    ground.position.y = -20;
    ground.rotation.x = -Math.PI / 2;
    ground.receiveShadow = true;
    this.context.scene.add(ground);
};
