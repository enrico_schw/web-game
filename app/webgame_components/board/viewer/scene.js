'use strict';

/**
 * This is the 3D scene constructor.
 * Notice it also constructions a bunch of the WebGL & Three resources necessary for the scene.
 *
 * This singleton allows for encapsulated access to all components of the 3D scene.
 *
 *
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene = function () {

    this.context = null;

    this.domElement = null;
    this.WIDTH = null;
    this.HEIGHT = null;

    this.scene = null;
    this.camera = null;
    this.lights = null;
    this.map = null;
    this.renderer = null;
    this.controls = null;
    this.clock = null;

    this.grid = null;

};

ShigGame.Viewer.Scene.prototype = {

    init: function (settings) {

        var params = {context: this};


        this.context = params.context;


        this.domElement = settings.element;
        this.WIDTH = settings.width;
        this.HEIGHT = settings.height;


        this.scene = new THREE.Scene();


        this.camera = new ShigGame.Viewer.Scene.Camera(params);
        this.lights = new ShigGame.Viewer.Scene.Lights(params);
        this.renderer = new ShigGame.Viewer.Scene.Renderer(params);

        this.map = new ShigGame.Viewer.Scene.Ground(params);


        // Events
        THREEx.WindowResize(this.renderer, this.camera.camObj);

        this.controls = new THREE.OrbitControls(this.camera.camObj, this.renderer.domElement);

        this.controls.target = new THREE.Vector3(0, 0, 0);

        this.clock = new THREE.Clock();

    },

    initGrid: function (gridFactory) {
        this.grid = gridFactory.createGrid(this.scene);
    },

    add: function (mesh) {
        this.scene.add(mesh);
    },

    render: function () {
        this.renderer.render(this.scene, this.camera.camObj);
    },

    update: function () {
        this.controls.update(this.clock.getDelta());
    }
}
;

