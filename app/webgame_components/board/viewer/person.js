'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace  First Person Camera Controls
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.Person = function (camera, params) {

    // First Person Camera Controls
    this.context = params.context;
    this.person = new THREE.Object3D();
    this.person.add(camera);
    this.person.position.set(16, 42, 16);
    this.person.rotation.set(0, -Math.PI / 2.0, 0);
};


