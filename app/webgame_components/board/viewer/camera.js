'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace  Camera initialization.  Contains setup for both Perspective and Orthographic cameras.
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.Camera = function (params) {

    this.context = params.context;

    // DevCamera
    this.camObj = new THREE.PerspectiveCamera(45, this.context.WIDTH / this.context.HEIGHT, 1, 10000);
    this.context.scene.add(this.camObj);

    this.camObj.position.set(-160, 60, 120);
    this.camObj.lookAt(new THREE.Vector3(0, 0, 0));


    // Game Camera
    this.cameraParObj = new THREE.Object3D();
    this.cameraParObj.position.y = 200;
    this.cameraParObj.position.z = 700;
    this.context.scene.add(this.cameraParObj);

    this.perspectiveCamera = new THREE.PerspectiveCamera(90, this.context.WIDTH / this.context.HEIGHT, 0.01, 1500);
    this.cameraParObj.add(this.perspectiveCamera);

};
