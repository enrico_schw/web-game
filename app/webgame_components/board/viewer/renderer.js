'use strict';

ShigGame.Viewer.Scene = ShigGame.Viewer.Scene || {};

/**
 * @namespace  Camera initialization.  Contains setup for both Perspective and Orthographic cameras.
 * @class Creates cameras for the scene.
 * @param params
 * @constructor
 */
ShigGame.Viewer.Scene.Renderer = function (params) {

    this.context = params.context;

    var renderer = new THREE.WebGLRenderer({antialias: true, alpha: false});
    renderer.setClearColor(0xffffff);
    renderer.setSize(this.context.WIDTH, this.context.HEIGHT);
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;

    this.context.domElement.append(renderer.domElement);
    return renderer;
};
