'use strict';

/**
 * Create a mesh and insert the geometry and the material. Translate the whole mesh
 * by 1.5 on the x axis and by 4 on the z axis and add the mesh to the scene.
 *
 * This singleton allows for encapsulated access to all components of the 3D scene.
 *
 *
 * @param params
 * @constructor
 */
Character.Square = function () {
    this.init();
};


Character.Square.prototype = new Character.Main();
Character.Square.prototype.constructor = Character.Square;


Character.Square.prototype.init = function () {
    var squareGeometry = new THREE.Geometry();
    squareGeometry.vertices.push(new THREE.Vector3(-1.0, 1.0, 0.0));
    squareGeometry.vertices.push(new THREE.Vector3(1.0, 1.0, 0.0));
    squareGeometry.vertices.push(new THREE.Vector3(1.0, -1.0, 0.0));
    squareGeometry.vertices.push(new THREE.Vector3(-1.0, -1.0, 0.0));
    squareGeometry.faces.push(new THREE.Face3(0, 1, 2));
    squareGeometry.faces.push(new THREE.Face3(0, 2, 3));

    var squareMaterial = new THREE.MeshBasicMaterial({
                color: 0x8080FF,

                side: THREE.DoubleSide
            }
        )
        ;

    this.mesh = new THREE.Mesh(squareGeometry, squareMaterial);
    this.mesh.position.set(1.5, 0.0, 4.0);
};


