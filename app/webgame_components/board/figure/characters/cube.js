'use strict';

/**
 * Create a mesh and insert the geometry and the material. Translate the whole mesh
 * by 1.5 on the x axis and by 4 on the z axis and add the mesh to the scene.
 *
 * This singleton allows for encapsulated access to all components of the 3D scene.
 *
 *
 * @param params
 * @constructor
 */
Character.Cube = function () {
    this.init();
};


Character.Cube.prototype = new Character.Main();
Character.Cube.prototype.constructor = Character.Cube;


Character.Cube.prototype.init = function () {

// Create the cube
    // Parameter 1: Width
    // Parameter 2: Height
    // Parameter 3: Depth

    var boxGeometry = new THREE.BoxGeometry(1.5, 1.5, 1.5);

    // Applying different materials to the faces is a more difficult than applying one
    // material to the whole geometry. We start with creating an array of
    // THREE.MeshBasicMaterial.

    // Define six colored materials

    var boxMaterials = [
        new THREE.MeshBasicMaterial({color: 0xFF0000}),
        new THREE.MeshBasicMaterial({color: 0x00FF00}),
        new THREE.MeshBasicMaterial({color: 0x0000FF}),
        new THREE.MeshBasicMaterial({color: 0xFFFF00}),
        new THREE.MeshBasicMaterial({color: 0x00FFFF}),
        new THREE.MeshBasicMaterial({color: 0xFFFFFF})
    ];

    // Create a MeshFaceMaterial, which allows the cube to have different materials on
    // each face

    var boxMaterial = new THREE.MeshFaceMaterial(boxMaterials);

    // Create a mesh and insert the geometry and the material. Translate the whole mesh
    // by 1.5 on the x axis and by 4 on the z axis and add the mesh to the scene.

    this.mesh = new THREE.Mesh(boxGeometry, boxMaterial);
    this.mesh.position.set(1.5, 0.0, 4.0);
};

//######################################################################################################################

Character.Cube2 = function () {
    this.init();
};


Character.Cube2.prototype = new Character.Main();
Character.Cube2.prototype.constructor = Character.Cube2;


Character.Cube2.prototype.init = function () {

// Create the cube
    // Parameter 1: Width
    // Parameter 2: Height
    // Parameter 3: Depth

    var boxGeometry = new THREE.BoxGeometry(2.0, 2.0, 2.0);

    // Applying different materials to the faces is a more difficult than applying one
    // material to the whole geometry. We start with creating an array of
    // THREE.MeshBasicMaterial.

    // Define six colored materials

    var crateTexture = new THREE.ImageUtils.loadTexture('textures/Crate.jpg');
    crateTexture.minFilter = THREE.LinearFilter;
    crateTexture.magFilter = THREE.LinearMipMapNearestFilter;

    // Create a material, which contains the texture.
    // Unfortunately, the CanvasRenderer doesn't support MeshLambertMaterial in combination
    // with Textures. Otherwise, the MeshBasicMaterial doesn't support lighting. As
    // compromise, the CanvasRenderer will show the texture without lighting via
    // MeshBasicMaterial.
    // Activate the 'doubleSided' attribute to force the rendering of both sides of each
    // face (front and back). This prevents the so called 'backface culling'. Usually,
    // only the side is rendered, whose normal vector points towards the camera. The other
    // side is not rendered (backface culling). But this performance optimization sometimes
    // leads to wholes in the surface. When this happens in your surface, simply set
    // 'doubleSided' to 'true'.

    var boxMaterial = new THREE.MeshLambertMaterial({
        map: crateTexture,
        side: THREE.DoubleSide
    });


    this.mesh = new THREE.Mesh(boxGeometry, boxMaterial);
    this.mesh.position.set(0.0, 0.0, 4.0);

};
