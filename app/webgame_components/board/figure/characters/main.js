'use strict';

/**
 * Create a mesh and insert the geometry and the material. Translate the whole mesh
 * by 1.5 on the x axis and by 4 on the z axis and add the mesh to the scene.
 *
 * This singleton allows for encapsulated access to all components of the 3D scene.
 *
 *
 * @param params
 * @constructor
 */
Character.Main = function () {
    this.mesh = null;
};


Character.Main.prototype.rotate = function () {
    this.mesh.rotation.x -= 0.075;
};
