'use strict';

ShigGame.Grid.Hexagon = ShigGame.Grid.Hexagon || {};

/**
 *
 * @constructor
 */
ShigGame.Grid.Hexagon.Shader = function () {

};

ShigGame.Grid.Hexagon.Shader.prototype.getVertexShader = function () {
    return 'void main() {\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n}';
};

ShigGame.Grid.Hexagon.Shader.prototype.getFragmentShader = function () {
    return 'void main() {\n\tgl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );\n}';
};
