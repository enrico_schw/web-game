'use strict';

/**
 *
 * @constructor
 */

ShigGame.Grid.Map = function (scene) {
    this.scene = scene;
    this.size = 10;
    this.hexagons = [];
};

ShigGame.Grid.Map.prototype = {

    init: function (columns, rows) {

        var height = this.size;
        var width = Math.sqrt(3) / 2 * height;

        var vertDistance = height * 3 / 4;
        var horizDistance = width;
        var insertion = 0;

        for (var row = 0; row < rows; row++) {
            insertion = (row % 2) ? 0 : width / 2;
            for (var col = 0; col < columns; col++) {
                var hex = ShigGame.Grid.Hexagon.create({
                    x: col * horizDistance + insertion,
                    y: 40,
                    z: row * vertDistance
                }, this.size);
                this.scene.add(hex.mesh);
            }
        }
    }
};

