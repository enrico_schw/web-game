'use strict';

/**
 *
 *
 *
 * @constructor
 */

ShigGame.Grid.Hexagon = function (center) {
    this.mesh = null;
    this.center = center;
    this.corners = [];

};


ShigGame.Grid.Hexagon.create = function (center, size) {

    var hexCorner = function (center, size, i) {
        var angle = 2 * Math.PI / 6 * (i + 0.5);

        return new THREE.Vector3(center.x + size * Math.cos(angle),
            center.y, center.z + size * Math.sin(angle));
    };

    var hexFaces = function (geometry) {
        geometry.faces.push(new THREE.Face3(0, 3, 1));
        geometry.faces.push(new THREE.Face3(1, 3, 2));
        geometry.faces.push(new THREE.Face3(3, 0, 4));
        geometry.faces.push(new THREE.Face3(0, 5, 4));

    };


    var createHexagon = function (center, size) {

        var squareGeometry = new THREE.Geometry();
        var shader = new ShigGame.Grid.Hexagon.Shader();

        var squareMaterial = new THREE.ShaderMaterial({
            vertexShader: shader.getVertexShader(),
            fragmentShader: shader.getFragmentShader()
        });

        var hex = new ShigGame.Grid.Hexagon(center);


        for (var i = 0; i < 6; i++) {
            hex.corners[i] = hexCorner(center, size, i);
            squareGeometry.vertices.push(hex.corners[i]);

        }
        squareGeometry.vertices.push(new THREE.Vector3(center.x, center.y, center.z));

        hexFaces(squareGeometry);
        hex.mesh = new THREE.Mesh(squareGeometry, squareMaterial);
        hex.mesh.position.set(center.x, center.y, center.z);

        return hex;
    };

    return createHexagon(center, size);
};


