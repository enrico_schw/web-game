'use strict';

/**
 * @ngdoc overview
 * @name WebGame game
 *
 * @description
 * This module contains the three js logic for the board
 *
 *
 */
/*jshint -W079, -W098*/
var ShigGame = {
    Viewer: {},
    Grid: {}
};

angular.module('boardModule', []);
