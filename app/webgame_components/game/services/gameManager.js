'use strict';

/**
 * @ngdoc overview
 * @name WebGame game gameManager
 *
 * @description
 * This service will support:
 *
 * Creating a new game
 * Handling the game loop/move action
 * Updating the score
 * Keeping track if the game is done
 *
 */
angular.module('gameModule')
    .service('GameManager', ['GridService', function (GridService) {
        // Create a new game
        this.newGame = function () {
        };
        // Handle the move action
        this.move = function () {
        };
        // Update the score
        this.updateScore = function (newScore) {
            this.newScore = newScore;
        };
        // Are there moves left?
        this.movesAvailable = function () {
            return GridService.anyCellsAvailable() ||
                GridService.tileMatchesAvailable();
        };
    }]);
