'use strict';

/**
 * @ngdoc overview
 * @name WebGame game gameManager
 *
 * @description
 * This service will support:
 *
 * Creating a new game
 * Handling the game loop/move action
 * Updating the score
 * Keeping track if the game is done
 *
 */
angular.module('gameModule')
    .service('GridService', function () {

        var grid = null;

        function assignGrid(gridObj) {
            grid = gridObj;
        }

        function getDimensions() {
            return {columns: 7, rows: 7};
        }

        return {
            assignGrid: assignGrid,
            getDimensions: getDimensions
        };
    });
