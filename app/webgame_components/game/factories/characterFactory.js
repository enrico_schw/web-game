'use strict';

angular.module('gameModule').
    factory('CharacterFactory', function () {

        /**
         * Initialize the 3D square.
         *
         */
        function createSquare() {
            return new Character.Square();
        }

        /**
         * Initialize the 3D square.
         *
         */
        function createCube() {
            return new Character.Cube2();
        }

        return {
            createSquare: createSquare,
            createCube: createCube
        };
    }
);
