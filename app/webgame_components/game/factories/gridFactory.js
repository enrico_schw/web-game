'use strict';

angular.module('gameModule').
    factory('GridFactory', ['GridService', function (GridService) {

        /**
         * Initialize the 3D grid.
         *
         */
        function createGrid(scene) {
            var dimensions = GridService.getDimensions();
            var grid = new ShigGame.Grid.Map(scene);

            grid.init(dimensions.columns, dimensions.rows);
            GridService.assignGrid(grid);

            //var grid = new HexGrid({
            //    size: 10,
            //    cellSize: 40,
            //    cellScale: 0.95
            //});
            //
            //scene.add(grid.group);

            return grid;
        }

        return {
            createGrid: createGrid
        };
    }]);
