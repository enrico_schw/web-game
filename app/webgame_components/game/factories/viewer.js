'use strict';

angular.module('gameModule').
    factory('Viewer', ['GridFactory', function (GridFactory) {
        //factory('Viewer', ['$timeout', function ($timeout) {

        var scene;

        /**
         * Initialize the 3D scene.
         * @param  {!{ canvasId: string, containerId: string }} params
         */
        function init(params) {
            scene = new ShigGame.Viewer.Scene();
            scene.init(params);
            scene.initGrid(GridFactory);
        }

        function addMesh(mesh) {
            scene.add(mesh);
        }

        function render() {
            scene.render();
        }

        function update() {
            scene.update();
        }

        return {
            init: init,
            addMesh: addMesh,
            render: render,
            update: update
        };
    }])
;
