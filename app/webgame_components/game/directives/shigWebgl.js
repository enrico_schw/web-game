'use strict';

angular.module('gameModule').
    //directive('shigWebgl', ['Viewer', 'CharacterFactory', function (Viewer, CharacterFactory) {
    directive('shigWebgl', ['ViewerDev', function (Viewer) {

        //var square = null;

        function link(scope, element) {

            var container = document.getElementById('board');

            Viewer.init({
                element: element,
                width: container.clientWidth,
                height: container.clientHeight < 450 ? 450 : container.clientHeight
            });


            //square = CharacterFactory.createCube();

            //Viewer.addMesh(square.mesh);
            Viewer.render();


            var animate = function () {

                requestAnimationFrame(animate);
                //square.rotate();
                Viewer.render();
                Viewer.update();

            };
            animate();
        }

        return {
            link: link,
            restrict: 'A',
            scope: {},
            templateUrl: 'webgame_components/game/views/shig-webgl.html'
        };
    }]);
