'use strict';

/**
 * @ngdoc overview
 * @name WebGame game
 *
 * @description
 * This module contains the game logic
 *
 *
 */
angular.module('gameModule', []);
