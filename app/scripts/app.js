'use strict';

/**
 * @ngdoc overview
 * @name shigwebgameApp
 * @description
 * # shigwebgameApp
 *
 * Main module of the application.
 *
 *
 */
angular
    .module('webgameApp', [
        'ngAnimate',
        'ngAria',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'gameModule',
        'boardModule',
        'boardDevModule',
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
