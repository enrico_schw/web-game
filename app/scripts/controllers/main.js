'use strict';

/**
 * @ngdoc function
 * @name webgameApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webgameApp
 */
angular.module('webgameApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
